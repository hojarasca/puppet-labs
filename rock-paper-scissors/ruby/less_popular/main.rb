require_relative 'puppet'
$stdout.sync = true

# start|<current_puppet_id>|<enemy_puppet_id>|<seed>
# update|<puppet_1>:<accion_puppet_1>|<puppet_2>:<accion_puppet_2>|<resultado>|<ganador></ganador>

def elegir_al_azar
  ['rock', 'paper', 'scissors'].sample
end

jugada_segura = nil
mi_nombre = nil

puppet = RPSPuppet.new

while true
  input = gets
  data = input.strip.split('|')
  if data[0] == 'start'
    srand data[3].to_i

    mi_nombre = data[1]
    su_nombre = data[2]
    
    new_action = puppet.on_start(mi_nombre, su_nombre)
    puts new_action
  else
    actions = Hash[[data[1], data[2]].map {|d| d.split(':')}]
    new_action = puppet.on_update(actions, data[3], data[4])
    puts new_action
  end
end