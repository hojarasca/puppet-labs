class RPSPuppet
  def initialize 
    @plays = {
      'rock' => 0,
      'paper' => 0,
      'scissors' => 0
    }
  end

  def on_start(my_name, their_name)
    @my_name = my_name
    @their_name = their_name
    @plays.keys.sample
  end

  def on_update(moves, result, winner)
    moves.values.each do |value|
      @plays[value] += 1
    end
    @plays.keys.min_by { |key| @plays[key] }
  end
end