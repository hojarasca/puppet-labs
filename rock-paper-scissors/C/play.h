#ifndef _PLAY_H_
#define _PLAY_H_

#include "rps.h"

#define ROCK_PAPER_SCISSORS 0

#define CURR_GAME ROCK_PAPER_SCISSORS


int play(void);
int playGame(const int game);

#endif
