#include "play.h"

int play(void) {
	return playGame(CURR_GAME);
}

int playGame(const int game) {
	switch(game) {
		case ROCK_PAPER_SCISSORS:
			return rockPaperScissors();
		default:
			perror("Game not found");
			return 1;
	}
}
