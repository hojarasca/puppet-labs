#ifndef _START_H_
#define _START_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "parser.h"

int parseStartingStatement(char** myName);

#endif
