#!/bin/sh
currDir=$(pwd)
tmux kill-session -t PuppetTest
tmux new-session -d -s PuppetTest -c $currDir "./build.sh && clear"
sleep 2
while read line
do
	tmux send $line ENTER
	sleep .25
done < ./tstFood.txt
