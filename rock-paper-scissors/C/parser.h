#ifndef _PARSER_H_
#define _PARSER_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct {
	char** strings;
	size_t length;
} serialized_t;

void freeSerialized(serialized_t** toDestroy);
int serializeInput(const char* input, serialized_t** into);
int parseLine(serialized_t** into);

#endif
