#ifndef _RPS_H_
#define _RPS_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "start.h"
#include "parser.h"

#define TRUE 1
#define FALSE 0

#define WINNER_IDX 3
#define WINNER_NAME_IDX 4

#define WINNER_TAG "winner"
#define TIED_TAG "tied"

/**
 * update|<puppet_1>:<accion_puppet_1>|<puppet_2>:<accion_puppet_2>|<resultado>|<ganador></ganador>
 * start|<puppet_id>|<seed>
 */

enum Result {
	Won,
	Lost, 
	Tied,
	End
};

char* chooseRps(int roundNmb);
enum Result playRound(const char * myName, int roundNmb);
int rockPaperScissors(void);

#endif
