const readline = require('readline');
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
  terminal: false
})

// start|<puppet_id>|<seed>
// update|<puppet_1>:<accion_puppet_1>|<puppet_2>:<accion_puppet_2>|<resultado>|<ganador></ganador>

let miNombre = null
let jugadaSegura = null

function elegirAlAzar () {
  items = ['rock', 'paper', 'scissors']
  return items[Math.floor(Math.random() * items.length)];
}

rl.on('line', function(line){
  data = line.trim().split('|')
  if (data[0] == 'start') {
    miNombre = data[1]
    jugadaSegura = elegirAlAzar()
    console.log(jugadaSegura)
  } else {
    const huboGanador = data[3] === 'winner'
    if (huboGanador && data[4] === miNombre) {
      console.log(jugadaSegura)
    } else {
      jugadaSegura = elegirAlAzar()
      console.log(jugadaSegura)
    }
  }
})