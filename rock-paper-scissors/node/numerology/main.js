const readline = require('readline');
const { onStart, onUpdate } = require('./puppet')
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
  terminal: false
})

// https://github.com/nodejs/node/issues/11568#issuecomment-282765300
if (process.stdout._handle) process.stdout._handle.setBlocking(true)

// start|<puppet_id>|<seed>
// update|<puppet_1>:<accion_puppet_1>|<puppet_2>:<accion_puppet_2>|<resultado>|<ganador></ganador>

rl.on('line', function(line){
  data = line.trim().split('|')
  if (data[0] == 'start') {
    const myName = data[1]
    const theirName = data[2]
    const seed = data[3]
    const move = onStart(myName, theirName, seed)
    console.log(move)
  } else {
    const actions = [data[1], data[2]].map(d => d.split(':')).reduce((prev, [key, value]) => ({...prev, key: value }))
    const result = data[3]
    const winner = data[4]
    const nextMove = onUpdate(actions, result, winner)
    console.log(nextMove)
  }
})