const gcd = (a, b) => {
  let divider = Math.max(a, b)
  let divisor = Math.min(a, b)
  while (divisor !== 0) {
    let temp = divider % divisor
    divider = divisor
    divisor = temp
  }
  return divider
}

const lcm = (a, b) => {
  return (a * b) / gcd(a, b)
}

const zip = (anArray, anotherArray) => {
  return anArray.map((element, index) => [element, anotherArray[index]])
}

/**
 * Magic connection with the gods. Added at the end of every
 * sample to connect it with the magic of the Carpincho god.
 */
const magicToken = `ñ`

let moves = null
let currentIndex = 0

/**
 * This algorithm was created by a carpincho wizzard with the
 * power to predict peoples interactions based only on their names.
 * 
 * It first connects the names with the magic of the carpincho wizzard
 * through a magic token. Then it split the names in their letters to 
 * be able to work with the fundamental pieces.
 * 
 * The next step is generate armony by creating 2 sequences of the same length.
 * 
 * Lastly it applies the powers of the equilibrium numbers to decide the
 * correct sequences of moves in order to win.
 * 
 * @param {string} myName 
 * @param {string} theirName 
 * @param {string} _seed 
 */
const onStart = (myName, theirName, _seed) => {
  // Adds the magic tokens
  myName = myName + magicToken
  theirName = theirName + magicToken

  // Splits in letters.
  myName = myName.split('')
  theirName = theirName.split('')

  // Generates sequences of the same length
  const finalLength = lcm(myName.length, theirName.length)
  myName = Array(finalLength / myName.length).fill(myName).flat()
  theirName = Array(finalLength / theirName.length).fill(theirName).flat()

  // Combines the 2 sequences and apply the equilibrim numbers to generate a
  // sequence of valid moves.
  moves = zip(myName, theirName).map(([myLetter, theirLetter]) => {
    const sum = myLetter.charCodeAt(0) + theirLetter.charCodeAt(0)
    const magicNumber = Math.floor(sum * 7 * Math.PI * ( (1 + Math.sqrt(5)) / 2 ))
    const result = magicNumber % 3
    switch (result) {
      case 0:
        return 'rock'
      case 1:
        return 'paper'
      case 2:
        return 'scissors'
    }
  })
  return onUpdate()
}


/**
 * All the moves were previously calculated, so the only thing left is play the next one.
 * @param {object} _moves 
 * @param {string} _result 
 * @param {string} _winner 
 */
const onUpdate = (_moves, _result, _winner) => {
  const res = moves[currentIndex]
  currentIndex = (currentIndex + 1) % moves.length
  return res
}

module.exports = {
  onStart,
  onUpdate
}