# On start se ejecuta cuando el juego empieza. Recibe el nombre de la
# puppet actual, el nombre de la puppet enemiga y la semilla de aleatoredad (que ya fue previamente seteada)
#
# On update se ejecuta al final de cada ronda.
# 
# En ambos casos se debe hacer un "print" al final para realizar la jugada.


# Esto es un mapa de las ocurrencias de cada jugada a lo largo del torneo.
historical_actions = {
  "rock": 0,
  "paper": 0,
  "scissors": 0
}

# Esta puppet empieza jugando roca porque es la mejor jugada.
def onStart(_my_name, _their_name, _seed):
  print('rock')


# Después recuerda las jugadas realizadas
# y luego busca la mas popular y la repite.
def onUpdate(my_action, their_action, _previous_winner):
  historical_actions[my_action] += 1
  historical_actions[their_action] += 1
  next_action = max(["rock", "paper", "scissors"], key=lambda action: historical_actions[action])
  print(next_action)