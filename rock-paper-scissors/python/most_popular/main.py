from random import seed
from puppet import onStart, onUpdate

## Format

# start|<current_puppet_id>|<enemy_puppet_id>|<seed>
# update|<puppet_1>:<accion_puppet_1>|<puppet_2>:<accion_puppet_2>|<resultado>|<ganador></ganador>

this_puppet = None
enemy_puppet = None

def elegir_al_azar():
  return choice(["rock", "paper", "scissors"])

def main():
  while True:
    data = input().strip().split("|")
    if data[0] == "start":
      [my_name, their_name, seed_value] = data[1:]
      this_puppet = my_name
      enemy_puppet = their_name
      seed(seed_value)
      onStart(my_name, their_name, seed)
    else:
      there_was_a_winner = data[3] == 'winner'
      actions = {}
      winner = None
      for action in data[1:3]:
        [puppet, action] = action.split(':')
        actions[puppet] = action
      if there_was_a_winner:
        winner = data[4]
      onUpdate(actions[this_puppet], actions[enemy_puppet], winner)

if __name__ == "__main__":
  main()